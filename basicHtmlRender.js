var http = require('http'); // require Node Http module
var fs   = require('fs');   // require Node File module

http.createServer((req, res) => {
  var content = fs.readFileSync(__dirname + '/basicHtml.html', 'utf8'); // reading the Html file
  var content = content.replace('{msg}', 'Hello World!!!!') // replacing the content of Html file => let's think about templating for simplicity
  res.writeHead(200, {'Content-Type' : 'text/html'}) // try replacing text/html with text/plain => it will render text content of the Html file
  res.end(content);
}).listen(3000, '127.0.0.1');