# HTTP

Making HTTP requests is a core functionality for modern languages and one of the first things many developers learn.

> `http` is a module in Node.js standard library, with which you can just plug and go without having to install external dependencies.

## Create your own server
```javascript
var http = require('http'); // require the Node Http module

// create a server
http.createServer((req, res) => {
  // writing status code and content type returned
  res.writeHead(200, {'Content-Type' : 'text/plain'});
  // ending the request with simple Hello World!
  res.end('Hello World!');
}).listen(3000, 'localhost'); // listen event appended the server on port 3000 and localhost [or 127.0.0.1]
```
*File* : basic.js

## Render HTML file with http
```javascript
var http = require('http'); // require Node Http module
var fs   = require('fs');   // require Node File module

http.createServer((req, res) => {
  var content = fs.readFileSync(__dirname + '/basicHtml.html', 'utf8'); // reading the Html file
  var content = content.replace('{msg}', 'Hello World!!!!') // replacing the content of Html file => let's think about templating for simplicity
  res.writeHead(200, {'Content-Type' : 'text/html'}) // try replacing text/html with text/plain => it will render text content of the Html file
  res.end(content);
}).listen(3000, '127.0.0.1');
```
*File* : basicHtmlRender.js

## Rendering HTML file using stream
```javascript
var http = require('http'); // require Node Http module
var fs = require('fs');     // require Node File module

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type' : 'text/html'});
  fs.createReadStream(__dirname + '/basicHtml.html').pipe(res); // creating a read stream and piping it to a write stream
}).listen(3000, 'localhost');
```
*File* : basicHtmlRenderUsingStream.js

## Working with JSON

```javascript
var http = require('http');

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type' : 'application/json'}); // set the Content-Type to application/json
  var obj = {
    a : 'Hello',
    b : 'World'
  };
  res.end(JSON.stringify(obj)); // serializing the obj
}).listen(3000, 'localhost');
```
*File* : workingWithJSON.js

A good [read](https://www.twilio.com/blog/2017/08/http-requests-in-node-js.html) on how to extend usage of http and different types.