var http = require('http'); // require Node Http module
var fs = require('fs');     // require Node File module

http.createServer((req, res) => {
  // req.url checks for the Route URL
  if(req.url === '/' ) {
    fs.createReadStream(__dirname + '/basicHtml.html').pipe(res);
  } 
  // if URL === /sendJson
  else if(req.url === '/sendJson') {
    var obj = {
      a : 'Hello',
      b : 'World!'
    }
    res.end(JSON.stringify(obj));
  }
  // any other URL passed
  else {
    res.writeHead(404); // sending a error header status
    res.end('Invalid Route!');
  }
}).listen(3000, 'localhost');