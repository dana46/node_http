var http = require('http'); // require the Node Http module

// create a server
http.createServer((req, res) => {
  // writing status code and content type returned
  res.writeHead(200, {'Content-Type' : 'text/plain'});
  // ending the request with simple Hello World!
  res.end('Hello World!');
}).listen(3000, 'localhost'); // listen event appended the server on port 3000 and localhost [or 127.0.0.1]