var http = require('http'); // require Node Http module
var fs = require('fs');     // require Node File module

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type' : 'text/html'});
  fs.createReadStream(__dirname + '/basicHtml.html').pipe(res); // creating a read stream and piping it to a write stream
}).listen(3000, 'localhost');