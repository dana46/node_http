var http = require('http');

http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type' : 'application/json'}); // set the Content-Type to application/json
  var obj = {
    a : 'Hello',
    b : 'World'
  };
  res.end(JSON.stringify(obj)); // serializing the obj
}).listen(3000, 'localhost');